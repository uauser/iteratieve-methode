function [u, flag, relRes, iter, resVec] = gewogenJacobi(A, b, u0, omega)
maxiter = 1000;
tol=10e-10;
iter = 0;
resVec=[];
u = u0;
r= A*u-b;

   
while ((norm(r)>tol) & (iter<maxiter))
    Dmin = diag(1./diag(A));
    u = ((eye(length(u0)))-omega*Dmin*A)*u + omega*Dmin*b;
    r = A*u-b;
    relRes = norm (r)/norm(b);
    resVec = [resVec norm(r)];
    iter = iter + 1;
end

flag = iter > maxiter;


end