%%
function [A, b] = generatePoisson(N)
    h = 1/(N+1);
    A = 1/h^2*diag(2*ones(N,1))+diag(-ones(N-1,1),1)+diag(-ones(N-1,1),-1);
    A = 1/h^2 *spdiags([-ones(N,1) 2*ones(N,1) -1*ones(N,1)],[-1 0 1],N,N);
    b=ones(N,1);
    b(1)=0;
    b(N)=0;
end