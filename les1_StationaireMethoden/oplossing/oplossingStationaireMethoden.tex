\input{../../header}

\title{Oplossing 1}
\subtitle{Stationaire iteraties}
\date{}


\begin{document}

\maketitle

\subsection*{Poisson-probleem}
Stel $\Omega=(0,1)$. We bekijken het Poisson-probleem met Dirichlet-randvoorwaarden
\begin{equation}\label{poisson}\tag{\ensuremath{\mathcal{P}}}
\begin{cases}
- \Delta u(x)  = f(x) \qquad \text{op } \Omega,\\
u(0) = u(1) = 0.
\end{cases}
\end{equation}

\begin{enumerate}
\item
Als we discretiseren met $N$ roosterpunten, wordt het domein $\Omega$ vervangen door $\Omega_h\dfn\{\,ih\,|\,i\in\{1,\dots,N\}\,\}$ met $h=(N+1)^{-1}$. We beschouwen een driepunt-discretisatie voor de operator $\Delta$.
\begin{equation}\label{poisson-disc}
-\Delta_h u_h = \frac{-(u_h)_{i-1}+2(u_h)_{i}-(u_h)_{i+1}}{h^2},
\end{equation}
wat orde van convergentie $2$ heeft (als $h$ overal constant is). 

\ \\
Het $(N\times N)$-stelsel, dat bij (\ref{poisson-disc}) hoort, is
\[
\underbrace{
h^{-2}
\begin{pmatrix}
2      & -1     & 0       & \cdots & 0\\
-1     & \ddots & \ddots  & \ddots & \vdots\\
0      & \ddots & \ddots  & \ddots & 0\\
\vdots & \ddots & \ddots  & \ddots & -1\\
0      & \cdots & 0       & -1     & 2
\end{pmatrix}
}_{\nfd A^{\mathrm{1D}}_h}
\begin{pmatrix}
(u_h)_1\\
\vdots\\
(u_h)_N
\end{pmatrix}
=
\begin{pmatrix}
(f_h)_1\\
\vdots\\
(f_h)_N
\end{pmatrix}.
\]

\item
\begin{lstlisting}
% =======================================================
% *** FUNCTION getPoissonMatrix
% ***
% *** Returns the 1D-Poisson-matrix and a right hand side
% *** (with f=1) for any given discretization parameter
% *** N.
% ***
% =======================================================
function [A,b] = getPoissonMatrix( N )


  h = 1/(N+1);

  e = ones( N,1 );
  A = h^(-2) ...
    * spdiags( [-e 2*e -e], [-1,0,1], N, N );

  b = ones(N,1);

end
% =======================================================
% *** END FUNCTION getPoissonMatrix
% =======================================================
\end{lstlisting}
\end{enumerate}


\subsection*{Richardson-iteratie}

We bekijken de \emph{Richardson-iteratie} gedefinieerd door
\[
x^{(k+1)} = (I - A) x^{(k)} + b
\]
voor een algemeen stelsel $Ax^*=b$.

\begin{enumerate}
\item De iteratie convergeert als en slechts als
\[
\rho(I-A)<1.
\]
Omdat voor de eigenwaarden van $M=I-A$ geldt
\[
\lambda(I-A) = 1 - \lambda(A),
\]
moet aan
\[
\max_{\lambda\in\sigma(A)}|1-\lambda| < 1
\]
voldaan zijn. Dit betekent niets anders dan dat alle eigenwaarden van $A$ in een bol met straal $1$ rond punt $1$ moeten liggen.


\item
\begin{lstlisting}
% =======================================================
% *** FUNCTION richardson
% ***
% *** Richardson method.
% ***
% =======================================================
function [ u, flag, relRes, iter, resVec ] ...
                                 = richardson( A, b, u0 )

  % initialization
  tol     = 1e-10;
  maxIter = 1e4;

  normB   = norm( b, 2 );
  u       = u0;
  res     = b - A*u;
  iter    = 0;

  resVec    = zeros( maxIter+1, 1 );
  resVec(1) = norm( res, 2 );

  % set default flag: everything all right
  flag = 0;

  % - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % start the loop
  relTol = tol*normB;
  for iter = 1:maxIter

      if resVec(iter) <= relTol
          break
      end

      % the actual update;
      % formulating the thing with 'res' removes the need
      % for *two* matrix-vector-multiplications per step
      u = u + res;

      res            = b - A*u;
      resVec(iter+1) = norm( res, 2 );
  end
  % - - - - - - - - - - - - - - - - - - - - - - - - - - -


  % - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % handle the output
  if iter>=maxIter
      flag = 1;
      warning ( 'richardson:maxIter', ...
                'Maximum number of iterations reached.');
  end
  resVec = resVec(1:iter+1);
  relRes = resVec(iter+1)/normB;
  % - - - - - - - - - - - - - - - - - - - - - - - - - - -

end
% =======================================================
% *** END FUNCTION richardson
% =======================================================
\end{lstlisting}

\item De Richardson-iteratie convergeert als en slechts als alle eigenwaarden van $A^{\mathrm{1D}}_h$ niet verder dan $1$ afstand van punt $1$ wegliggen. We moeten dus eisen dat
\[
\lambda_N = \frac{4}{h^2} \sin^2\left(\frac{N\pi}{2(N+1)}\right) \stackrel{!}{<} 2.
\]
Hieruit kunnen we concluderen dat de Richardson-iteratie voor geen enkele $N$ zal convergeren.

\end{enumerate}


\subsection*{Gewogen Jacobi-iteratie}

We bekijken de \emph{gewogen Jacobi-iteratie} gedefinieerd door
\[
x^{(k+1)} = (I - \omega D^{-1}A) x^{(k)} + \omega D^{-1} b
\]
met $\omega>0$ voor een algemeen stelsel $Ax^*=b$.

\begin{enumerate}
\item

\begin{lstlisting}
% ========================================================
% *** FUNCTION jacobiRelaxed
% ***
% *** Relaxed Jacobi method, relaxation parameter tau.
% ***
% ========================================================
function [ u, flag, relRes, iter, resVec ] = ...
		jacobiRelaxed( A, b, u0, omega )

  % initialization
  tol     = 1e-10;
  maxIter = 1e3;

  normB   = norm( b, 2 );
  u       = u0;
  res     = b - A*u;
  iter    = 0;

  N = length(b);

  Dinv = spdiags(1./spdiags(A,0),0,N,N);
  I = speye(N);
  R = (I-omega*(Dinv*A));

  resVec    = zeros( maxIter+1, 1 );
  resVec(1) = norm( res, 2 );

  % set default flag: everything all right
  flag = 0;

  % - - - - - - - - - - - - - - - - - - - - - - - - - - 
  % start the loop
  relTol = tol*normB;
  for iter = 1:maxIter

      %if resVec(iter) <= relTol | In comment to compare
      %    break                 | for all iterations
      %end                       | (do this for next questions)

      % the actual update;
      % formulating the thing with 'res' removes the need 
      % for *two* matrix-vector-multiplications per step
      u = R*u +omega*(Dinv*b);

      res            = b - A*u;
      resVec(iter+1) = norm( res, 2 );
  end
  % - - - - - - - - - - - - - - - - - - - - - - - - - - 


  % - - - - - - - - - - - - - - - - - - - - - - - - - - 
  % handle the output
  if iter>=maxIter
      flag = 1;
      warning ( 'jacobiRelaxed:maxIter', ...
                'Maximum number of iterations reached.' );
  end
  relRes = resVec(iter+1)/normB;
  % - - - - - - - - - - - - - - - - - - - - - - - - - - 

end
% ======================================================
% *** END FUNCTION jacobiRelaxed
% ======================================================
\end{lstlisting}

\item
\begin{enumerate}
\item In lemma 1.17 wordt duidelijk, dat we $\lambda_{\mathrm{max}}(D^{-1}A)$ moeten bekijken. Omdat $D = 2h^{-2} I$ geldt
\[
D^{-1}A = \frac{h^2}{2} A,
\]
\[
\lambda_k(D^{-1}A) = 2 \sin^2\left(\frac{k\pi}{2(N+1)}\right);
\]
\[
\lambda_{\mathrm{max}}(D^{-1}A) = 2 \sin^2\left(\frac{N\pi}{2(N+1)}\right).
\]
We kiezen dus
\[
\omega\in \biggl(0, \frac{1}{\sin^2\left(\frac{N\pi}{2(N+1)}\right)} \bigg).
\]
\item % (a)
$R_{Jac,\omega} = I -\omega D^{-1} A = I-\omega A h^2/2$ of
{\footnotesize
\begin{eqnarray*}
R_{Jac,\omega} &=& \left(\begin{array}{cccccccc}
1  & 0 &  0   & 0  & 0  &  0  &  0  & 0\\
0   &1  & 0   &  0  & 0&  0  & 0   &  0  \\
0   &  0   &1  & 0 &  0  & 0&  0     & 0\\
0   &  0   & 0  &1  & 0 &  0  & 0   &  0\\
0   &  0   & 0  &  0 &1  & 0 &  0    & 0\\
0   &  0   &  0  & 0  &  0 &1  & 0   &  0\\
0   & 0   &  0   &  0   & 0  &  0 &1  & 0 \\
0  & 0    & 0    &  0   &  0   & 0  &  0 &1
\end{array}\right)
\\
&& -\omega
\left(
\begin{array}{cccccccc}
1  & -1/2 &  0   & 0  & 0  &  0  &  0  & 0\\
-1/2   &1  & -1/2   &  0  & 0&  0  & 0   &  0  \\
0   &  -1/2   &1  & -1/2 &  0  & 0&  0     & 0\\
0   &  0   & -1/2  &1  & -1/2 &  0  & 0   &  0\\
0   &  0   & 0  &  -1/2 &1  & -1/2 &  0    & 0\\
0   &  0   &  0  & 0  &  -1/2 &1  & -1/2   &  0\\
0   & 0   &  0   &  0   & 0  &  -1/2 &1  & -1/2 \\
0  & 0    & 0    &  0   &  0   & 0  &  -1/2 &1
\end{array}\right)
\end{eqnarray*}
}
    \item
 De eigenvectoren en eigenwaarden worden gevonden met het commando
 {\tt [v,d] = eig(R)} in Matlab. De numerieke eigenwaarden die we
 vinden zijn $\{ \pm 0.9397,\pm 0.7667,\pm 0.5000,\pm 0.1736 \}$ en de
 eigenvectoren worden getoond in Figuur \ref{vraag_3b}.
\begin{figure}[H]
\begin{center} 
\resizebox{10cm}{10cm}{\includegraphics{vraag_3b.pdf}}
\caption{De eigenvectoren van de Jacobi-iteratie (of gewogen Jacobi met $\omega=1$). Merk op dat de eigenvectoren zogenaamde {\it staande} golven zijn op het interval [0,1]. Ze zijn, op een normalisatie na, $\sin(k \pi x)$ met $k=1,\ldots, 8$. \label{vraag_3b}}
\end{center}
\end{figure}
Herinner verder ook de analytische formules voor de eigenvectoren en eigenwaarden
uit de cursus.  De analytische eigenvectoren zijn, op een normalisatie
na, $v_k = \sin(k \pi x)$ met $k=1,\ldots,8$.  De bijhorende
eigenwaarden zijn $1- 2 \omega \sin^2(k \pi/(2 (N+1))$.

\item 
Figuur \ref{vraag_3d_res} toont de norm van het residu voor de verschillende
rechterhanden. Het convergentiegedrag wordt gedomineerd door de grootste eigenwaarde, in de ontbinding in eigenvectoren van de iteratiematrix $R$.

Omdat we starten met een initi\"ele schatting $u_0=0$, is $u_1$ bepaald door $D^{-1}b$. $D^{-1}$ is een constante dus werkt de iteratiematrix direct op een veelvoud van $b$. Het is daarom interessant om te kijken hoe $b$ ontbindt in eigenvectoren.

De rechterhand $b=\sin(k \pi x)$ bestaat uit slechts \'e\'en enkele eigenvector.  Voor $k=1$ hoort deze bij de
grootste eigenwaarde. Voor $k=2,3,4$ is de eigenwaarde kleiner en is er
snellere convergentie.

Wanneer men de verhouding van het residu in twee opeenvolgende
iteraties uitrekent, vindt men een convergentiefactor van $0.9397$
voor $k=1$, $0.7660$ voor $k=2$ en $0.500$ voor $k=3$.  Dit komt
ongeveer overeen met de drie grootste eigenwaarden van de iteratiematrix.

De rechterhand $f(x)=x(1-x)$, en dus ook de initi\"ele fout, is slechts te ontbinden met behulp van alle even eigenvectoren, diegene die horen bij $k=1,3,5,\ldots$. 
\tiny{
\begin{equation*}
f_N(x)=\sum_{n=1}^N c_n \sin(n\pi x),
\end{equation*}
met $c_n=0$ voor even $n$.
}
\normalsize{
Hierbij hoort ook de eigenvector die hoort bij de grootste
eigenwaarde. Wanneer we de verhouding van het residu in twee opeenvolgende iteraties uitrekenen, vinden we een convergentiefactor van $0.9397$.}
\end{enumerate}
\end{enumerate}

\begin{figure}[H]
\begin{center}
\includegraphics[width=10cm]{vraag_3d_res.pdf}
\caption{Het verloop van de norm van het residu voor verschillende rechterhanden $f$. \label{vraag_3d_res}}
\end{center}
\end{figure}

\end{document}
