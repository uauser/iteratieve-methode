% =========================================================================
% *** FUNCTION jacobiRelaxed
% ***
% *** Relaxed Jacobi method, relaxation parameter omega.
% ***
% =========================================================================
function [ u, flag, relRes, iter, resVec ] = jacobiRelaxed( A, b, u0, omega )

  % initialization
  tol     = 1e-10;
  maxIter = 1e3;

  normB   = norm( b, 2 );
  u       = u0;
  res     = b - A*u;
  iter    = 0;

  N = length(b);

  Dinv = spdiags(1./spdiags(A,0),0,N,N);
  I = speye(N);
  R = (I-omega*(Dinv*A));

  resVec    = zeros( maxIter+1, 1 );
  resVec(1) = norm( res, 2 );

  % set default flag: everything all right
  flag = 0;

  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % start the loop
  relTol = tol*normB;
  for iter = 1:maxIter

      %if resVec(iter) <= relTol
      %    break
      %end

      % the actual update;
      % formulating the thing with 'res' removes the need for *two*
      % matrix-vector-multiplications per step
      u = R*u +omega*(Dinv*b);

      res            = b - A*u;
      resVec(iter+1) = norm( res, 2 );
  end
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  % handle the output
  if iter>=maxIter
      flag = 1;
      warning ( 'jacobiRelaxed:maxIter', ...
                'Maximum number of iterations reached.' );
  end
  relRes = resVec(iter+1)/normB;
  % - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

end
% =========================================================================
% *** END FUNCTION jacobiRelaxed
% =========================================================================