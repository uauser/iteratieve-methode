function u = richardson( A, b, u0 )

n = size(A,1);
I = ones(n,n);
for i=1:n
for j=1:n
if i==j
I(i,j) = 1;
else
I(i,j) = 0;
end
end
end

first=1;
u = u0;
while norm(b-A*u,2) > 1e-10*norm(b,2);
u = (I-A)*u + b;
end